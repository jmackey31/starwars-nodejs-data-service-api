// app.js
const express           = require('express'),
    app                 = express(),
    bodyParser          = require('body-parser'),
    cors                = require('cors'),
    morgan              = require('morgan'),
    mongoose            = require('mongoose'),
    config              = require('./config/config'),
    path                = require('path'),
    methodOverride      = require('method-override');

const API_PREFIX_VAL = '/starwars_v1';

if (process.env.ENV_STATUS === 'local') {
    console.log('------ "' + process.env.ENV_STATUS + '" env ------');
    // Use Cors to handle cors issues in dev env.
    app.use(cors());

    // Log all requests to the console
    app.use(morgan('dev'));
}
app.use(cors());
app.use(morgan('dev'));
app.use(methodOverride());

// APP CONFIGURATION
// use body parser so we can grap information from POST requests
// Also need to set the nginx configuration file settings.
// http://www.codingslover.com/2015/05/413-request-entity-too-large-error-with.html
// vim /etc/nginx/nginx.conf
// # set client body size to 20M #
// client_max_body_size 20M;
// service nginx restart
app.use(express.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}));
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.set('X-Powered-By', process.env.API_POWERED_BY);
    next();
});

// Configure our app to handle CORS requests
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, \ Authorization');
    next();
});

// Connect to the database
mongoose.connect(config.database, { useNewUrlParser: true })
    .then(() => {
        console.log('mongoDB is connected...');
    })
    .catch((err) => {
        console.log('Mongo not connected.');
        console.log(err);
    });

// API ROUTES -------------------------
let apiRoutes = require('./app/routes/api')(app, express);
app.use(API_PREFIX_VAL, apiRoutes);

// START THE SERVER
app.listen(config.port);
console.log(`Listening on: http://localhost:${config.port}${API_PREFIX_VAL}`);

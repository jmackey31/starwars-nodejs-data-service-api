const   jwt                 = require('jsonwebtoken'),
        async               = require('async'),
        crypto              = require('crypto'),
        config              = require('../../config/config'),
        qr                  = require('qr-image'),
        sg                  = require('sendgrid')(process.env.SENDGRID_API_KEY),
        qrcode              = require('qrcode-js'),
        superSecret         = config.jwt_secret;

const   User              = require('../models/user'),
        ContactUsFeedback = require('../models/contact-us-feedback'),
        USCityState       = require('../models/uscitystate'),
        ErrorLogDoc       = require('../models/error-log-doc'),
        JobTaskDoc        = require('../models/job-task-doc');

const todays = new Date();

// -----------------------------------------------------------------
let globalFunctionsModule = {};

// -----------------------------------------------------------------
globalFunctionsModule.emailProcessFunction = (args) => {
    let email_content = args.email_content;
    let helper = require('sendgrid').mail,
        from_email = new helper.Email(args.from_email),
        to_email = new helper.Email(args.to_email),
        subject = args.subject,
        content = new helper.Content('text/plain', email_content),
        mail = new helper.Mail(from_email, subject, to_email, content);

    let request = sg.emptyRequest({
        method: 'POST',
        path: 'https://api.sendgrid.com/v3/mail/send',
        body: mail.toJSON()
    });

    sg.API(request, function (error, response) {
        if (error) {
            return {
                success: false,
                message: 'Your message could not be sent.',
                error: error
            };
        } else {
            return {
                success: true,
                message: 'Your message has been sent!',
                response: response
            };
        }
    });
};

// -----------------------------------------------------------------
globalFunctionsModule.returnParamsAndBody = (req, res) => {
    return res.json({
        success: true,
        message: 'Return params and body.',
        reqParams: req.params,
        reqBody: req.body,
        reqQuery: req.query
    });
};

// -----------------------------------------------------------------
globalFunctionsModule.forgotPasswordUser = (req, res) => {
    let sentEmailToReset = req.body.email.toLowerCase();
    const current_website_string = process.env.ENV_STATUS === 'local' ? config.env_local_website_url_string : config.env_api_string;

    // console.log(`${config.env_api_string}/reset-password/${user_code}/${verify_code}`);
    // console.log(`${config.env_api_string}/reset-password/user_code/verify_code`);

    async.waterfall([
            function(done) {
                crypto.randomBytes(20, function(err, buf) {
                    let token = buf.toString('hex');
                    done(err, token);
                });
            },
            function(token, done) {
                User.findOne({ email: sentEmailToReset }, function(err, user) {
                    if (!user) {
                        return res.json({
                            success: false,
                            message: 'Sorry, we could not locate any user with that email address.',
                        });
                    } else if (err) {
                        return res.json({
                            success: false,
                            message: 'Something went wrong.  There was an error.',
                            err: err
                        });
                    } else {
                        user.email_verify_password_token = token;
                        user.email_verify_password_expires = Date.now() + 3600000; // 1 hour

                        user.save(err => {
                            done(err, token, user);
                        });
                    }
                });
            },
            function(token, user, done) {
                const reset_url_with_token = `${current_website_string}/reset-password/${user.user_code}/${token}`;
                console.log(reset_url_with_token);
                let helper      = require('sendgrid').mail,
                    from_email  = new helper.Email(process.env.ADMIN_EMAIL),
                    to_email    = new helper.Email(sentEmailToReset),
                    subject     = 'StarWars Fan Club | Password Reset',
                    content     = new helper.Content('text/plain', '"' + user.username + '"\n\n' +
                        'We have received a request to reset your account password.\n\n' +
                        'Reset your password here: ' + reset_url_with_token + ' \n\n' +
                        'Your Reset Token: "' + token + '"\n\n' +
                        'Your Username: "' + user.username + '"\n\n' +
                        'This information will expire shortly.".\n'),
                    mail        = new helper.Mail(from_email, subject, to_email, content);

                let request = sg.emptyRequest({
                    method: 'POST',
                    path: 'https://api.sendgrid.com/v3/mail/send',
                    body: mail.toJSON()
                });

                sg.API(request, function(error, response) {
                    if (error) {
                        return res.json({
                            success: false,
                            message: 'Your message could not be sent.  There was an error.',
                            error: error
                        });
                    } else {
                        return res.json({
                            success: true,
                            message: 'Please check your email for further instructions on resetting your password.',
                            response: response
                        });
                    }
                });
            }
        ],
        function(err) {
            return res.json({
                success: false,
                message: 'Something went wrong.  There was an error.',
                err
            });
        });
};

// -----------------------------------------------------------------
globalFunctionsModule.resetUserPassword = (req, res) => {
    async.waterfall([
            function(done) {
                const resetQuery = {
                    'user_code': req.params.user_code,
                    'reset_password_token': req.params.token,
                    'reset_password_expires': { $gt: Date.now() }
                };

                User.findOne(resetQuery, function(err, user) {
                    if (err) {
                        return res.json({
                            success: false,
                            message: 'Password could not be reset.  There was an error.',
                            err: err
                        });
                    } else if (!user) {
                        return res.json({
                            success: false,
                            message: 'Your password reset token is invalid or has expired.'
                        });
                    } else {
                        user.password                   = req.body.new_password;
                        user.reset_password_token       = '';
                        user.reset_password_expires     = '';

                        // return res.json({
                        //     success: true,
                        //     message: 'Testing resetting user password.',
                        //     user: user,
                        //     reqBody: req.body,
                        //     reqParams: req.params
                        // });

                        user.save(function(err) {
                            if (err) {
                                return res.json({
                                    success: false,
                                    message: 'Password could not be reset.  There was an error.',
                                    err: err
                                });
                            } else {
                                let helper      = require('sendgrid').mail,
                                    from_email  = new helper.Email(process.env.ADMIN_EMAIL),
                                    to_email    = new helper.Email(user.email),
                                    subject     = 'Your password has been reset successfully!',
                                    content     = new helper.Content('text/plain', 'Hello,\n\n' +
                                        'This is a confirmation to inform you that your password has just been changed.\n'),
                                    mail        = new helper.Mail(from_email, subject, to_email, content);

                                let request = sg.emptyRequest({
                                    method: 'POST',
                                    path: 'https://api.sendgrid.com/v3/mail/send',
                                    body: mail.toJSON()
                                });

                                sg.API(request, function(error, response) {
                                    if (error) {
                                        return res.json({
                                            success: false,
                                            message: 'Your message could not be sent.  There was an error.',
                                            error: error
                                        });
                                    } else {
                                        return res.json({
                                            success: true,
                                            message: 'You may now log in with your new password',
                                            username: user.username,
                                            response: response
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            },
        ],
        function(err) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Something went wrong.  There was an error.',
                    err: err
                });
            }
        });
};

// -----------------------------------------------------------------
globalFunctionsModule.submitContactUsFeedback = (req, res) => {
    // return res.json({
    //     success: false,
    //     message: 'Testing the waters.',
    //     reqBody: req.body,
    //     reqParams: req.params
    // });

    let feedback = new ContactUsFeedback();

    feedback.contact_us_code                                        = createDynamicCode('contact_us_feedback');
    if (req.body.user_id) feedback.user_id                          = req.body.user_id;
    if (req.body.user_code) feedback.user_code                      = req.body.user_code;
    if (req.body.username) feedback.username                        = req.body.username;
    if (req.body.contact_us_message) feedback.contact_us_message    = req.body.contact_us_message;
    if (req.body.contact_us_category) feedback.contact_us_category  = req.body.contact_us_category;
    if (req.body.city) feedback.city                                = req.body.city;
    if (req.body.state) feedback.state                              = req.body.state;
    feedback.created_on                                             = todays;

    feedback.save(err => {
        if (err) {
            return {
                success: false,
                message: 'Could not submit your feedback.  There was an error.',
                err
            };
        } else {
            return res.json({
                success: true,
                message: 'Thank you for your feedback.',
                feedback
            });
        }
    });
};

// -----------------------------------------------------------------
globalFunctionsModule.submitContactUsFeedbackV2 = (req, res) => {
    // return res.json({
    //     success: false,
    //     message: 'Testing the waters v2.',
    //     reqBody: req.body,
    //     reqParams: req.params
    // });

    let feedback = new ContactUsFeedback();

    feedback.contact_us_code                                        = createDynamicCode('contact_us_feedback');
    if (req.body.user_id) feedback.user_id                          = req.body.user_id;
    if (req.body.user_code) feedback.user_code                      = req.body.user_code;
    if (req.body.email) feedback.email                              = req.body.email;
    if (req.body.username) feedback.username                        = req.body.username;
    if (req.body.your_name) feedback.sender_name                    = req.body.your_name;
    if (req.body.contact_us_message) {
        feedback.contact_us_message    = req.body.contact_us_message;
    } else if (req.body.your_message) {
        feedback.contact_us_message    = req.body.your_message;
    }
    if (req.body.contact_us_category) {
        feedback.contact_us_category  = req.body.contact_us_category;
    } else if (req.body.message_type) {
        feedback.contact_us_category  = req.body.message_type;
    }
    if (req.body.city) feedback.city                                = req.body.city;
    if (req.body.state) feedback.state                              = req.body.state;
    feedback.created_on                                             = todays;

    feedback.save(err => {
        if (err) {
            return {
                success: false,
                message: 'Could not submit your message.  There was an error.',
                err
            };
        } else {
            return res.json({
                success: true,
                message: 'Thank you for your feedback.',
                feedback
            });
        }
    });
};

// -----------------------------------------------------------------
globalFunctionsModule.logGlobalErrorsForReviewLocal = (args) => {
    function sendErrorToMCAdminViaEmail(emailArgs) {
        let email_content;

        if (emailArgs.error_not_logged === true) {
            email_content = 'Your error logging service is not working (not saving to the database).  Please check this out immediately.';
        } else {
            email_content = 'An error that needs your attention has been logged:  error_id_code: "' + args.error_log.error_id_code + '".';
        }

        let helper = require('sendgrid').mail,
            from_email = new helper.Email('me@joshuamackey.com'),
            to_email = new helper.Email(process.env.ADMIN_EMAIL),
            subject = 'MC Error Logged For Reference.  Please check this out.',
            content = new helper.Content('text/plain', email_content),
            mail = new helper.Mail(from_email, subject, to_email, content);

        let request = sg.emptyRequest({
            method: 'POST',
            path: 'https://api.sendgrid.com/v3/mail/send',
            body: mail.toJSON()
        });

        sg.API(request, function (error, response) {
            if (error) {
                console.log(error);
                console.log('Sendgrid error above: -----------');
                const error_from_sendgrid = {
                    success: false,
                    message: 'Your message could not be sent.',
                    error: error
                };

                return {
                    error_from_sendgrid: error_from_sendgrid
                }
            } else {
                const success_response_from_sendgrid = {
                    success: true,
                    message: 'Your message has been sent!',
                    response: response
                };

                return {
                    success_response_from_sendgrid: success_response_from_sendgrid
                }
            }
        });
    }

    let error_log = new ErrorLogDoc();

    error_log.error_id_code                                         = createDynamicCode('error_id_code');
    if (args.needs_attention === true) {
        error_log.needs_attention = args.needs_attention;
    }
    if (args.user_id) error_log.user_id                             = args.user_id;
    if (args.user_code) error_log.user_code                         = args.user_code;
    if (args.vendor_id) error_log.vendor_id                         = args.vendor_id;
    if (args.vendor_code) error_log.vendor_code                     = args.vendor_code;
    if (args.error_data_object) error_log.error_data_object         = args.error_data_object;
    error_log.created                                               = todays;

    // Save the error record and check for errors
    error_log.save(function(err) {
        let errorLogArgs;

        if (err) {
            errorLogArgs = {
                success: false,
                error_not_logged: true,
                message: 'Could not log this error.  There was an error.',
                err: err,
                error_log: error_log
            };

            sendErrorToMCAdminViaEmail(errorLogArgs);
        } else {
            // Contact an MC admin via email about the error
            errorLogArgs = {
                success: true,
                message: 'Error was logged successfully!',
                error_log: error_log
            };

            sendErrorToMCAdminViaEmail(errorLogArgs);
        }
    });
};

globalFunctionsModule.logGlobalErrorsForReviewGlobal = args => {
    // // JUST CREATE A JOB THAT SENDS OUT THE EMAIL ONCE OR TWICE A DAY SO THAT YOU DON'T GET KILLED WITH
    // // EMAIL SERVICE EXPENSES.
    let error_log = new ErrorLogDoc();

    error_log.error_id_code                                         = createDynamicCode('error_id_code');
    if (args.needs_attention === true) {
        error_log.needs_attention = args.needs_attention;
    }
    if (args.user_id) error_log.user_id                             = args.user_id;
    if (args.user_code) error_log.user_code                         = args.user_code;
    if (args.error_data_object) error_log.error_data_object         = args.error_data_object;
    error_log.created                                               = todays;

    // Save the error record and check for errors
    error_log.save(function(err) {
        // let errorLogArgs;

        if (err) {
            return {
                success: false,
                error_not_logged: true,
                message: 'Could not log this error.  There was an error.',
                err: err,
                error_log: error_log
            };

            // sendErrorToMCAdminViaEmail(errorLogArgs);
        } else {
            // Contact an MC admin via email about the error
            return {
                success: true,
                message: 'Error was logged successfully!',
                error_log: error_log
            };

            // sendErrorToMCAdminViaEmail(errorLogArgs);
        }
    });
};

globalFunctionsModule.logGlobalJobTaskForReviewGlobal = (args) => {
    // JUST CREATE A JOB THAT SENDS OUT THE EMAIL ONCE OR TWICE A DAY SO THAT YOU DON'T GET KILLED WITH
    // EMAIL SERVICE EXPENSES.

    let job_task = new JobTaskDoc();

    job_task.job_task_id_code                                      = createDynamicCode('job_task_id_code');
    if (args.user_id) job_task.user_id                             = args.user_id;
    if (args.user_code) job_task.user_code                         = args.user_code;
    if (args.job_task_data_object) job_task.job_task_data_object   = args.job_task_data_object;
    job_task.created                                               = todays;

    // Save the job task record and check for errors
    job_task.save(function(err) {
        // let errorLogArgs;

        if (err) {
            return {
                success: false,
                job_task_not_logged: true,
                message: 'Could not log this error.  There was an error.',
                err: err,
                job_task: job_task
            };

            // sendErrorToMCAdminViaEmail(errorLogArgs);
        } else {
            // Contact an MC admin via email about the error
            return {
                success: true,
                message: 'Error was logged successfully!',
                job_task: job_task
            };

            // sendErrorToMCAdminViaEmail(errorLogArgs);
        }
    });
};

// -----------------------------------------------------------------
globalFunctionsModule.getDNSValueFromURL = (req, res) => {
    // Code refrence from "https://nodejs.org/docs/latest-v7.x/api/dns.html#dns_dns_getservers"
    const dns = require('dns');
    const options = {
        family: 6,
        hints: dns.ADDRCONFIG | dns.V4MAPPED,
    };

    let dnsPromise = new Promise((resolve, reject) => {
        let myLookup = dns.lookup(req.params.url_string, options, (err, address, family) => {
            if (err) {
                resolve(err);
            } else {
                let finalObj = {
                    url: req.params.url_string,
                    address: address,
                    family: family
                };
                resolve(finalObj);
            }
        });
    });

    let allOptions = new Promise((resolve, reject) => {
        options.all = true;
        dns.lookup(req.params.url_string, options, (err, addresses) => {
            if (err) {
                resolve(err);
            } else {
                let finalObj = {
                    url: req.params.url_string,
                    addresses: addresses
                };
                resolve(finalObj);
            }
        });
    });

    Promise.all([
        dnsPromise,
        allOptions
    ])
    .then(function(values) {
        return res.json({
            success: true,
            message: 'Your data is in!',
            dataset: values
        });
    })
    .catch(reason => {
        return res.json({
            success: false,
            message: 'There was a complication with executing your request.',
            reason: reason
        });
    });
};

// -----------------------------------------------------------------
createDynamicCode = codeNamePrefix => {
    const getTimeNow = Date.parse(new Date());
    const randNum = Math.random().toString();
    const slicedNum = randNum.slice(3,9);

    return codeNamePrefix + '_' + getTimeNow + slicedNum;
};

// -----------------------------------------------------------------
globalFunctionsModule.createDynamicTrimCode = () => {
    const getTimeNow = Date.parse(new Date());
    const timeString = getTimeNow.toString();

    return timeString.slice(0,10);
};

// -----------------------------------------------------------------
globalFunctionsModule.createDynamicCode = codeNamePrefix => {
    const getTimeNow = Date.parse(new Date());
    const randNum = Math.random().toString();
    const slicedNum = randNum.slice(3,9);

    return codeNamePrefix + '_' + getTimeNow + slicedNum;
};

// -----------------------------------------------------------------
globalFunctionsModule.sixDigitCode = () => {
    const randNum = Math.random().toString();
    return randNum.slice(3,9);
};

// ----------------------------------------------------------------------
globalFunctionsModule.checkFoulWordsDontExist = word_string => {
    // My bad language checker
    const bad_words = [
        'fuck', 'fucker', 'shit', 'shitdick', 'bitch', 'cunt', 'cuntbox', 'ass', 'asshole', 'dick'
    ];
    const found_bad_words = [];

    // console.log('Bad words are being checked.');
    // console.log(bad_words.length);

    if (!word_string || !bad_words) {
        // console.log('this failed bud!');

        return false;
    } else {
        for (let i = 0, len = bad_words.length; i < len; i++) {
            if (word_string.match(new RegExp(bad_words[i], 'gi'))) {
                found_bad_words.push(bad_words[i]);
            }
        }

        // console.log('found_bad_words below: -------------');
        // console.log(found_bad_words.length);
        // console.log(found_bad_words);

        return found_bad_words.length <= 0;
    }

    // wordFinder('I like to pull a bad chick every now and then.');
    // wordFinder('I like to pull a bad bitch every now and then.  Assholes eat pussy fart shit balls.');
};

// ----------------------------------------------------------------------
globalFunctionsModule.svgCodeGenerator = args => {
    function createItemCode() {
        let getTimeNow = Date.parse(new Date());
        let randNum = Math.random().toString();
        let slicedNum = randNum.slice(3,9);

        // return args+'_'+getTimeNow+slicedNum;
        return `${args}_${getTimeNow}${slicedNum}`;
    }

    let generatedCode = createItemCode();

    return qr.imageSync(generatedCode, {type: 'svg'});
};

// ----------------------------------------------------------------------
globalFunctionsModule.base64ImageIdCodeGenerator = (pre_made_code) => {
    // function createItemCode() {
    //     let getTimeNow = Date.parse(new Date());
    //     let randNum = Math.random().toString();
    //     let slicedNum = randNum.slice(3,9);
    //
    //     return args+'_'+getTimeNow+slicedNum;
    // }
    //
    // let generated_code_string = createItemCode();

    // return qrcode.toDataURL(generated_code_string, 4);
    // return qrcode.toDataURL(generated_code_string, 8);
    // return qrcode.toDataURL(generated_code_string, 16);
    return qrcode.toDataURL(pre_made_code, 4);
};

globalFunctionsModule.base64ImageIdCodeGeneratorFromPreMadeCode = (pre_made_code) => {
    return qrcode.toDataURL(pre_made_code, 4);
};

globalFunctionsModule.base64ImageIdCodeGeneratorFromPreMadeCode = (pre_made_code) => {
    return qrcode.toDataURL(pre_made_code, 4);
};

globalFunctionsModule.testBase64ImageIdCodeGenerator = (req, res) => {
    function createItemCode(args) {
        let getTimeNow = Date.parse(new Date());
        let randNum = Math.random().toString();
        let slicedNum = randNum.slice(3,9);

        return args+'_'+getTimeNow+slicedNum;
    }

    let generated_code_string = createItemCode(req.body.prefix_value);
    // console.log('generated_code_string ----------------');
    // console.log(generated_code_string);
    // console.log('generated_code_string ----------------');

    let finalImageString = qrcode.toDataURL(generated_code_string, 4);

    return res.json({
        success: true,
        message: 'Checkout things...',
        prefix_value: req.body.prefix_value,
        generated_code_string: generated_code_string,
        finalImageString: finalImageString
    });
};

globalFunctionsModule.createBase64ImageIdFromCode = (req, res) => {
    let finalImageString = qrcode.toDataURL(req.body.code, 4);

    return res.json({
        success: true,
        message: 'Checkout things...',
        code: req.body.code,
        finalImageString: finalImageString
    });
};

// -----------------------------------------------------------------
globalFunctionsModule.testAPI = (req, res) => {
    return res.json({
        success: true,
        message: 'Thank your for testing this service.',
        words: [
            { word: 'Spaceships' },
            { word: 'The Dark Side' },
            { word: 'The Force' },
            { word: 'Best Fight Scenes' }
        ]
    });
};

// -----------------------------------------------------------------
globalFunctionsModule.useMiddlewareWithToken = (req, res, next) => {
    // check header or url parameters or post parameters for token
    let token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, superSecret, (err, decoded) => {
            if (err) {
                return res.status(403).json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(403).json({
            success: false,
            message: 'No token provided.'
        });
    }
};

// -----------------------------------------------------------------
globalFunctionsModule.authenticateUserWithToken = username => {
    return jwt.sign({
        username: username
    }, superSecret, {
        expiresIn: '72h' // expires in 72 hours
    });
};

// -----------------------------------------------------------------
globalFunctionsModule.authenticateUser = (req, res) => {
    const lower_username = req.body.username ? req.body.username.toLowerCase() : false;
    const lower_password = req.body.password ? req.body.password.toLowerCase() : false;

    let userLoginQuery = {
        'username': lower_username
    };

    User.findOne(userLoginQuery)
        .exec((err, user) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Could not log user in.  There was an error.',
                    err
                });
            } else if (user) {
                // check if password matches
                let myPasswordIsGood = lower_password ? user.comparePassword(lower_password) : false;

                if (!myPasswordIsGood) {
                    return res.json({
                        success: false,
                        message: 'Authentication failed.  Wrong password.'
                    });
                } else {
                    // if user is found and password is right
                    // create a token
                    let token = jwt.sign({
                        username: user.username
                    }, superSecret, {
                        expiresIn: '24h' // expires in 72 hours
                    });

                    // return the information including token as JSON
                    return res.json({
                        success: true,
                        user: {
                            user_token: token,
                            user_id: user._id,
                            user_code: user.user_code,
                            username: user.username,
                            firstname: user.firstname,
                            lastname: user.lastname,
                            profile_image: `http://www.facetheforce.today/random/400?r=1`,
                            email: user.email
                        }
                    });
                }
            } else {
                return res.json({
                    success: false,
                    message: 'Could not find your account.'
                });
            }
        });
};

// -----------------------------------------------------------------
globalFunctionsModule.getStates = (req, res) => {
    const usaStates = [];

    USCityState.find({}, { '_id': false })
        .select('state')
        .exec((err, states) => {
            if (err) {
                return res.json({
                    success: false,
                    err,
                    message: 'Could not get US States.'
                });
            } else if (states) {
                // console.log('Raw states count');
                // console.log(states.length);
                // Only allow 1 state name per record to go into
                // the above declared array
                states.forEach(function(item) {
                    // console.log(item);
                    // if (usaStates.indexOf(item.state.toLowerCase()) === -1) {
                    //     usaStates.push(item.state.toLowerCase());
                    // }
                    if (usaStates.indexOf(item.state) === -1) {
                        usaStates.push(item.state);
                    }
                });
                // console.log(usaStates);
                return res.json({
                    success: true,
                    message: 'Here are the US States.',
                    stateCount: usaStates.length,
                    usaStates: usaStates
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not get US States.'
                });
            }
        });
};

// -----------------------------------------------------------------
globalFunctionsModule.getCitiesInState = (req, res) => {
    USCityState.find({'state': req.params.state.toUpperCase()}, { '_id': false })
        .select('city')
        .sort({'city': 1})
        .exec((err, cities) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Could not get cities in the selected state: "'+req.params.state+'".',
                    err
                });
            } else if (cities) {
                return res.json({
                    success: true,
                    message: 'Here are the cities in the selected state: "'+req.params.state+'".',
                    cityCount: cities.length,
                    cities: cities
                });
            } else {
                return res.json({
                    success: false,
                    message: 'Could not get cities in the selected state: "'+req.params.state+'".'
                });
            }
        });
};

// -----------------------------------------------------------------
module.exports = globalFunctionsModule;

const todays = new Date();

const Character              = require('../models/character'),
    GlobalFunctions     = require('./global-functions');
const characters = require('../data/characters-list');

let characterLogicModule = {};

characterLogicModule.fetchAllPaginatedCharacters = (req, res) => {
    let page = (req.params.page !== undefined) ? Number(req.params.page) : 1;
    let limit = (req.params.limit !== undefined) ? Number(req.params.limit) : 5;
    const query = {};
    const options = { page, limit };

    Character.paginate(query, options, (err, result) => {
        if (err) {
            return res.json({
                success: false,
                message: 'Could not find any characters.  There was an error.',
                err
            });
        } else if (result.docs.length > 0) {
            const pageRange = [];
            const filteredDocs = result.docs.map(item => {
                return {
                    character_code:     item.character_code,
                    name:               item.name,
                    slug_name:          item.slug_name,
                    description:        item.description,
                    img_url:            item.img_url
                };
            });

            for (let i = 1; i < result.pages + 1; i++) {
                pageRange.push(i);
            }

            return res.json({
                success: true,
                message: 'Found your characters.',
                characters: filteredDocs,
                pagination: {
                    limit: result.limit,
                    page: result.page,
                    pages: result.pages,
                    total: result.total,
                    page_range: pageRange
                }
            });
        } else {
            return res.json({
                success: false,
                message: 'Could not find any characters.'
            });
        }
    });
};

characterLogicModule.fetchCharacterByCharacterCode = (req, res) => {
    Character.findOne({'character_code': req.params.character_code})
        .exec((err, item) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Could not find any matching characters.',
                    err
                });
            } else if (item) {
                return res.json({
                    success: true,
                    message: 'Found character.',
                    character: {
                        character_code:     item.character_code,
                        name:               item.name,
                        slug_name:          item.slug_name,
                        description:        item.description,
                        img_url:            item.img_url
                    }
                });
            } else {
                console.log('No dice.');
                return res.json({
                    success: false,
                    message: 'Could not find any matching characters.'
                });
            }
        });
};

characterLogicModule.seedCharacterDataIntoDB = (req, res) => {
    const createANewCharacter = arg => {
        const newCharacterCode = GlobalFunctions.createDynamicCode('character');
        let character = new Character();

        if (newCharacterCode && arg.name && arg.slug_name && arg.description && arg.img_url) {
            character.character_code    = newCharacterCode;
            character.name              = arg.name;
            character.slug_name         = arg.slug_name;
            character.description       = arg.description;
            character.img_url           = arg.img_url;
            character.created_on        = todays;

            character.save(err => {
                if (err) {
                    console.log(err);
                    console.log('Could not complete this operation.  Something went wrong.  -------------------------');

                    return false;
                } else {
                    console.log('-----------------  SUCCESS ON THE NEW CHARACTER!  -------------------------');

                    return true;
                }
            });
        } else {
            console.log('Not all values present.  NO Dice!!!  -------------------------');
        }
    };

    // Loop through all of the objects in the characters array and seed them.
    for (let i = 0; i < characters.length; i++) {
        // console.log(characters[i]);

        createANewCharacter(characters[i]);
    }

    return res.json({
        success: true,
        message: 'Database is now seeded with characters.'
    });
};

module.exports = characterLogicModule;

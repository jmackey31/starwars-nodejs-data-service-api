const todays = new Date();

const Message              = require('../models/message'),
    GlobalFunctions     = require('./global-functions'),
    ServerValidation     = require('./server-validation');

let messageLogicModule = {};

messageLogicModule.createNewMessage = (req, res) => {
    const user_id = ServerValidation.minimumCharacterLengthsCheck(8, req.params.user_id) ? req.params.user_id : false;
    const user_code = ServerValidation.minimumCharacterLengthsCheck(8, req.params.user_code) ? req.params.user_code : false;
    const full_name = ServerValidation.minimumCharacterLengthsCheck(2, req.body.full_name) ? req.body.full_name : false;
    const message_string = ServerValidation.minimumCharacterLengthsCheck(2, req.body.message) ? req.body.message : false;
    const base64_message_image_string = ServerValidation.validateAnyValue(req.body.base64_message_image_string) ? req.body.base64_message_image_string : false;
    const newMessageCode = GlobalFunctions.createDynamicCode('message');

    let message = new Message();

    if (newMessageCode &&
        user_id &&
        user_code &&
        full_name &&
        message_string &&
        base64_message_image_string) {
        message.message_code                    = newMessageCode;
        message.user_id                         = user_id;
        message.user_code                       = user_code;
        message.full_name                       = full_name;
        message.message                         = message_string;
        message.base64_message_image_string     = base64_message_image_string;
        message.created_on                      = todays;

        message.save(err => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Could not submit your message.  There was an error.',
                    err
                });
            } else {
                return res.json({
                    success: true,
                    message: 'Your message was created successfully.',
                    saved_message: message
                });
            }
        });
    } else {
        return res.json({
            success: false,
            message: 'Could not submit your message.  Please provide all of the necessary values.',
            valid_user_id: user_id,
            valid_user_code: user_code,
            valid_full_name: full_name,
            valid_message: message_string,
            valid_base64_message_image_string: base64_message_image_string,
            requestParams: req.params,
            requestBody: req.body
        });
    }
};

messageLogicModule.fetchAllPaginatedMessages = (req, res) => {
    let page = (req.params.page !== undefined) ? Number(req.params.page) : 1;
    let limit = (req.params.limit !== undefined) ? Number(req.params.limit) : 5;
    const query = {};
    const options = { page, limit };

    Message.paginate(query, options, (err, result) => {
        if (err) {
            return res.json({
                success: false,
                message: 'Could not find any messages.  There was an error.',
                err
            });
        } else if (result.docs.length > 0) {
            const pageRange = [];
            const filteredDocs = result.docs.map(item => {
                return {
                    message_code:                   item.message_code,
                    user_code:                      item.user_code,
                    full_name:                      item.full_name,
                    message:                        item.message,
                    base64_message_image_string:    item.base64_message_image_string
                };
            });

            for (let i = 1; i < result.pages + 1; i++) {
                pageRange.push(i);
            }

            return res.json({
                success: true,
                message: 'Found your messages.',
                messages: filteredDocs,
                pagination: {
                    limit: result.limit,
                    page: result.page,
                    pages: result.pages,
                    total: result.total,
                    page_range: pageRange
                }
            });
        } else {
            return res.json({
                success: false,
                message: 'Could not find any messages.'
            });
        }
    });
};

module.exports = messageLogicModule;

const todays = new Date();
const   config              = require('../../config/config'),
        jwt                 = require('jsonwebtoken'),
        superSecret         = config.jwt_secret;

const User              = require('../models/user'),
    GlobalFunctions     = require('./global-functions'),
    ServerValidation     = require('./server-validation');

let userLogicModule = {};

userLogicModule.registerNewUser = (req, res) => {
    const prepped_email = req.body.email ? req.body.email.toLowerCase() : false;
    const trimmed_email = prepped_email ? prepped_email.trim() : false;
    const final_email_check = ServerValidation.emailValidationCheck(trimmed_email) ? trimmed_email : false;
    const prepped_username = req.body.username ? req.body.username.toLowerCase() : false;
    const trimmed_username = prepped_username ? prepped_username.trim() : false;
    const final_username_check = ServerValidation.minimumCharacterLengthsCheck(8, trimmed_username) ? trimmed_username : false;
    const prepped_password = req.body.password ? req.body.password.toLowerCase() : false;
    const trimmed_password = prepped_password ? prepped_password.trim() : false;
    const final_password_check = ServerValidation.minimumCharacterLengthsCheck(8, prepped_password) ? prepped_password : false;

    const storeNewUserRecord = () => {
        const newUserCode = GlobalFunctions.createDynamicCode('user');
        let user = new User();

        user.user_code                                      = newUserCode;
        if (req.body.email) user.email                      = trimmed_email;
        if (req.body.username) user.username                = trimmed_username;
        if (req.body.password) user.password                = trimmed_password;
        user.base64_image_id_code                           = GlobalFunctions.base64ImageIdCodeGeneratorFromPreMadeCode(newUserCode);
        user.created_on                                     = todays;

        user.save(err => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Could not complete this operation.  Something went wrong.',
                    err
                });
            } else {
                const emailArgs = {
                    subject: 'StarWars fan club account Sign Up.',
                    from_email: process.env.ADMIN_EMAIL,
                    to_email: user.email,
                    email_content: `Thank you for creating an account with us.`
                };
                GlobalFunctions.emailProcessFunction(emailArgs);

                // Give the user a token and return their user info.
                const token = jwt.sign({
                    username: user.username
                }, superSecret, {
                    expiresIn: '24h' // expires in 72 hours
                });

                // return the information including token as JSON
                return res.json({
                    success: true,
                    message: 'Signup successfull!',
                    user: {
                        user_token: token,
                        user_id: user._id,
                        user_code: user.user_code,
                        username: user.username,
                        firstname: user.firstname,
                        lastname: user.lastname,
                        profile_image: `http://www.facetheforce.today/random/400?r=1`,
                        email: user.email
                    }
                });
            }
        });
    };

    const userQueryCheck = (args, callback) => {
        User.findOne(args.query)
            .exec((err, user) => {
                if (err) {
                    return res.json({
                        success: false,
                        message: 'Could not complete this operation.  There was an error.',
                        err
                    });
                } else if (!user) {
                    callback({ success: true });
                } else if (user) {
                    return res.json({
                        success: false,
                        message: args.message
                    });
                } else {
                    return res.json({
                        success: false,
                        message: 'Could not complete this operation.'
                    });
                }
            });
    };

    const checkUsernameDoesNotExist = () => {
        const args = {
            query: { 'username': trimmed_username },
            message: 'This "Username" is already taken.  Please select another one.'
        };
        userQueryCheck(args, check_valid => {
            if (check_valid.success === true) {
                storeNewUserRecord();
            }
        });
    };

    const checkEmailDoesNotExist = () => {
        const args = {
            query: { 'email': trimmed_email },
            message: 'This "Email Address" is already taken.  Please select another one.'
        };
        userQueryCheck(args, check_valid => {
            if (check_valid.success === true) {
                checkUsernameDoesNotExist();
            }
        });
    };

    // Check to make sure all of the values are valid before continuing.
    if (final_email_check && final_username_check && final_password_check) {
        checkEmailDoesNotExist();
    } else {
        return res.json({
            success: false,
            message: 'Please ensure that your data is valid.  Email (valid email), Username (8 characters), and Password (8 characters) need to be valid.',
            your_submitted_values: {
                email_valid: final_email_check,
                email: prepped_email,
                username_valid: final_username_check,
                username: prepped_username,
                password_valid: final_password_check,
                password: prepped_password
            }
        });
    }
};

module.exports = userLogicModule;

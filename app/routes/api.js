const   jwt               = require('jsonwebtoken'),
        config            = require('../../config/config'),
        superSecret       = config.jwt_secret;

const {
    testAPI,
    returnParamsAndBody,
    authenticateUser,
} = require('../logic/global-functions');
const {
    registerNewUser
} = require('../logic/userlogic');
const {
    // seedCharacterDataIntoDB,
    fetchAllPaginatedCharacters,
    fetchCharacterByCharacterCode
} = require('../logic/character-logic');
const {
    createNewMessage,
    fetchAllPaginatedMessages
} = require('../logic/message-logic');

module.exports = (app, express) => {
    const API_ROUTER = express.Router(null);

    // ------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------
    // NON AUTHENTICATED General / Other requests
    // ------------------------------------------------------------------------------
    API_ROUTER.get('/test', testAPI);
    API_ROUTER.post('/test2/:param1/:param2/:param3', returnParamsAndBody);
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    // NON AUTHENTICATED User requests
    // ------------------------------------------------------------------------------
    // POST -------------------------------------------------------------------------
    API_ROUTER.post('/user/register', registerNewUser);
    API_ROUTER.post('/user/authenticate', authenticateUser);
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    API_ROUTER.get('/', (req, res) => {
        return res.json({ message: 'You made it to the "Fictitious fan club".'});
    });

    // ------------------------------------------------------------------------------
    // Route middleware to verify tokens sent up with http and https requests
    API_ROUTER.use((req, res, next) => {
        // check header or url parameters or post parameters for token
        let token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, superSecret, (err, decoded) => {
                if (err) {
                    return res.json({
                        success: false,
                        message: 'Failed to authenticate.  You were logged out for security reasons.',
                        status: 403,
                        err: err,
                        token_error: err.name
                    });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            return res.json({
                success: false,
                status: 403,
                message: 'No token provided.'
            });
        }
    });

    // ------------------------------------------------------------------------------
    // AUTHENTICATED Characters requests
    // ------------------------------------------------------------------------------
    // GET --------------------------------------------------------------------------
    // API_ROUTER.get('/characters/seed-database', seedCharacterDataIntoDB); // Only use this when it's needed.
    API_ROUTER.get('/characters/all/:page/:limit', fetchAllPaginatedCharacters);
    API_ROUTER.get('/character/:character_code', fetchCharacterByCharacterCode);
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    // AUTHENTICATED Messages requests
    // ------------------------------------------------------------------------------
    // GET --------------------------------------------------------------------------
    API_ROUTER.get('/messages/all/:page/:limit', fetchAllPaginatedMessages);
    // POST --------------------------------------------------------------------------
    API_ROUTER.post('/message/new/:user_id/:user_code', createNewMessage);
    // ------------------------------------------------------------------------------

    return API_ROUTER;
};

const   mongoose = require('mongoose');
const   mongoosePaginate = require('mongoose-paginate'),
    Schema = mongoose.Schema;

let MessageSchema = new Schema({
    message_code: { type: String },
    user_id: { type: String },
    user_code: { type: String },
    full_name: { type: String },
    message: { type: String },
    base64_message_image_string: { type: String },
    created_on: { type: Date }
});

MessageSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Message', MessageSchema);

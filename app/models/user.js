const   mongoose = require('mongoose');
const   Schema = mongoose.Schema;
const   bcrypt = require('bcrypt-nodejs');

let UserSchema = new Schema({
    user_code: { type: String },
    svg_id_code: { type: String },
    base64_image_id_code: { type: String },
    username: { type: String, index: { unique: true } },
    password: { type: String, required: true },
    email: { type: String },
    phone: { type: String },
    full_name: { type: String },
    firstname: { type: String },
    lastname: { type: String },
    profile_image: { type: String },
    email_verify_password_token: { type: String },
    email_verify_password_expires: { type: Date },
    reset_password_token: { type: String },
    reset_password_expires: { type: Date },
    formattedDate: { type: String },
    created_on: { type: Date },
    updated_on: [Date]
});

// hash the password before the user is saved
UserSchema.pre('save', function(next) {
    let user = this;

    // hash the password only if the password has been changed or user is new
    if (!user.isModified('password')) return next();

    // generate the hash
    // NEED TO USE A REGULAR FUNCTION RATHER THAN AN ARROW FOR THE CALLBACK.
    bcrypt.hash(user.password, null, null, function(err, hash) {
        if (err) return next(err);

        // change the password to the hashed version
        user.password = hash;
        next();
    });
});

UserSchema.methods.comparePassword = function(password) {
    let user = this;

    return bcrypt.compareSync(password, user.password);
};

module.exports = mongoose.model('User', UserSchema);

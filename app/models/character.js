const   mongoose = require('mongoose');
const   mongoosePaginate = require('mongoose-paginate'),
    Schema = mongoose.Schema;

let CharacterSchema = new Schema({
    character_code: { type: String },
    name: { type: String },
    slug_name: { type: String },
    description: { type: String },
    img_url: { type: String },
    created_on: { type: Date }
});

CharacterSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Character', CharacterSchema);

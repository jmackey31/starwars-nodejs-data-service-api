const   mongoose = require('mongoose');
const   mongoosePaginate = require('mongoose-paginate');
const   Schema = mongoose.Schema;;

let ContactUsFeedbackSchema = new Schema({
    contact_us_status: { type: Number, required: true, default: 411 },
    contact_us_code: { type: String },
    user_id: { type: String },
    user_code: { type: String },
    email: { type: String },
    username: { type: String },
    sender_name: { type: String },
    contact_us_message: { type: String },
    contact_us_category: { type: String },
    city: { type: String },
    state: { type: String },
    formattedDate: { type: String },
    created_on: { type: Date },
    updated_on: [Date]
});

ContactUsFeedbackSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('ContactUsFeedback', ContactUsFeedbackSchema);

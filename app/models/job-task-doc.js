const   mongoose = require('mongoose');
const   mongoosePaginate = require('mongoose-paginate'),
        Schema = mongoose.Schema;

// JobTaskDoc schema
const JobTaskDocSchema = new Schema({
    job_task_id_code: {type: String},
    user_id: {type: String},
    user_code: {type: String},
    base64_image_id_code: {type: String},
    job_task_data_object: [],
    doc_type: {type: String, default: 'job_task_log_document', select: false},
    formattedDate: {type: String, select: false},
    created: {type: Date, default: Date.now, select: false}
});

JobTaskDocSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('JobTaskDoc', JobTaskDocSchema);

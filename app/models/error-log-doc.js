const   mongoose = require('mongoose');
const   mongoosePaginate = require('mongoose-paginate'),
        Schema = mongoose.Schema;

// ErrorLogDoc schema
const ErrorLogDocSchema = new Schema({
    error_id_code: {type: String},
    needs_attention: {type: Boolean, default: false},
    user_id: {type: String},
    user_code: {type: String},
    base64_image_id_code: {type: String},
    error_data_object: [],
    doc_type: {type: String, default: 'error_log_document', select: false},
    formattedDate: {type: String, select: false},
    created: {type: Date, default: Date.now, select: false}
});

ErrorLogDocSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('ErrorLogDoc', ErrorLogDocSchema);

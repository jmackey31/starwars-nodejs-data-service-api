'use strict';

const   mongoose    = require('mongoose'),
        Schema      = mongoose.Schema;

const   USCityStateSchema = new Schema({
        city: { type: String, index: { unique: true } },
        state: { type: String, index: { unique: true } }
});

module.exports = mongoose.model('USCityState', USCityStateSchema);

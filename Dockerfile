# NodeJS Back End Service Dockerfile
FROM keymetrics/pm2:latest-alpine

EXPOSE 4702

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package.json package.json
COPY pm2.json .

ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install --production

# Copy everything from my host into the image.
COPY . .

CMD ["pm2-runtime", "start", "pm2.json"]

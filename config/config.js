let ENV_DB_STRING;
let ENV_API_STRING;
let ENV_WEBSITE_STRING;

// Check to see which ENV_STATUS value is set
if (process.env.ENV_STATUS === 'local') {
    ENV_DB_STRING = process.env.LOCAL_DEV_DB_STRING;
    ENV_API_STRING = `http://localhost:${process.env.URL_PORT}`;
    ENV_WEBSITE_STRING = `http://localhost:${process.env.LOCAL_WEBSITE_URL_PORT}`;
    console.log('Local env so set the db to local');
} else if (process.env.ENV_STATUS === 'production') {
    ENV_DB_STRING = process.env.PRODUCTION_DB_STRING;
    ENV_API_STRING = 'https://joshuadata.com';
    console.log('Local env so set the db to production');
}

module.exports = {
    port: process.env.URL_PORT,
    database: ENV_DB_STRING,
    jwt_secret: process.env.JWT_SECRET,
    env_api_string: ENV_API_STRING,
    env_local_website_url_string: ENV_WEBSITE_STRING,
};

// server.js
const   http    = require('http');
const   dotenv  = require('dotenv').config(),
        config  = require('./config/config');
const   app     = require('./app');
